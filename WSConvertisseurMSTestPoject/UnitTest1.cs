using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using WSConvertisseur.Controllers;
using WSConvertisseur.Models;

namespace WSConvertisseurMSTestPoject
{
    [TestClass]
    public class UnitTest1
    {
        private DeviseController _controller = new DeviseController();

        [TestMethod]
        public void GetById_ExistingIdPassed_ReturnsOkObjectResult()
        {
            // Act
            var result = _controller.GetById(1).Result;
            // Assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult), "Pas un OkObjectResult");
        }

        [TestMethod]
        public void GetById_ExistingIdPassed_ReturnsRightItem()
        {
            // Act
            var result = _controller.GetById(1).Result as OkObjectResult;
            // Assert
            Assert.IsInstanceOfType(result.Value, typeof(Devise), "Pas une Devise");
            Assert.AreEqual(new Devise(1, "Dollar", 1.08), (Devise)result.Value, "Devises pas identiques");
 }
        [TestMethod]
        public void GetById_UnknownGuidPassed_ReturnsNotFoundResult()
        {
            // Act
            var result = _controller.GetById(20).Result;
            // Assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult), "Pas un NotFoundResult");
        }

        [TestMethod]
        public void getAll_ReturnsEnumerable()
        {
            var res = _controller.GetAll().Result;
            Assert.IsInstanceOfType(res, typeof(IEnumerable<Devise>), "pas une collection");
        }

        [TestMethod]
        public void getAll_ReturnsDevises()
        {
            var res = _controller.GetAll().Result as IEnumerable<Devise>;
            CollectionAssert.AllItemsAreInstancesOfType(res.ToList(), typeof(Devise), "ne contient pas des devises");
        }

        [TestMethod]
        public void getAll_ReturnsEnumerableNotEmpyy()
        {
            var res = _controller.GetAll().Result as IEnumerable<Devise>;
            Assert.AreNotEqual(res.ToList().Count, 0, "liste vide");
        }
    }
}
